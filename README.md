# mi_biosignals_public

Öffentlicher Bereich der Lehrveranstaltung Biosignale 
- Bsc. Angewandte Informatik, Schwerpunkt Medizinische Informatik

# Einbindung der Notebooks in jupyterlab

## Repository klonen (Einmal zu Beginn der LV)

#### In Jupyterlab einwählen: https://jupyter-cloud.gwdg.de

#### Git-Symbol rechts oben in der Menüleiste auswählen (siehe Bild)

![jupyter-git-button](images/jupyter-git.png)

#### Pfad des Repositories eingeben 

`https://gitlab.gwdg.de/medinfpub/mi_oer/mi_biosignals_public.git`

und "CLONE" auswählen (siehe Bild).

![jupyter-git-publicrepo](images/jupyter-git-publicrepo.png)

**Ergebnis: Das Verzeichnis mi_biosignals_pub wird im Homeverzeichnis angezeigt**

###### Troubleshooting

     Fehlermeldung "Cloning into 'mi_biosignals_public'... fatal: protocol ' https' is not supported" 

Leerzeichen vor dem https löschen




## Respository updaten (zu Beginn jeder Lehreinheit)

Da die Notebooks erst kurz vor der jeweiligen Lehreinheit veröffentlicht werden, müssen Sie, wenn Sie einen neuen Übungszettel bearbeiten wollen, das Repository updaten (pullen). 

#### In das Repository wechseln = in den Ordner mi_biosignals_pub wechseln

Sie können dies an dem Pfad oben im Fileexplorer erkennen (im Bild grün)

#### Im Menü oben Git auswählen und im Untermenü "Pull from Remote" auswählen

Im Bild rot markiert

![jupyter-git-pull](images/jupyter-git-pull.png)

**Ergebnis: In Ihrem Verzeichnis werden die neuen Dateien angezeigt.**

## Notebooks 

Zur Abgabe (im Stud.IP) laden Sie die Notebooks herunterladen

#### Auf korrekte 



